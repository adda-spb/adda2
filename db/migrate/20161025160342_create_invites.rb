class CreateInvites < ActiveRecord::Migration[5.0]
  def change
    create_table :invites do |t|
      t.references :group, foreign_key: true
      t.integer :recipient_id
      t.integer :sender_id

      t.timestamps
    end
  end
end
