Rails.application.routes.draw do
  mount GraphiQL::Rails::Engine, at: '/graphiql', graphql_path: '/graphql'
  post 'graphql' => 'graph_ql#execute'
  post 'user_token' => 'user_token#create'
end
