class User < ApplicationRecord
  has_secure_password
  has_many :invitations, class_name: 'Invite', foreign_key: 'recipient_id'
  has_many :sent_invites, class_name: 'Invite', foreign_key: 'sender_id'
  has_one :group

  validates :password, length: { minimum: 8 }
end
