GroupType = GraphQL::ObjectType.define do
  name 'Group'
  description 'Group type'

  interfaces [NodeIdentification.interface]
  
  global_id_field :id
  field :updated_at, types.String
  field :created_at, types.String
  field :user_id, types.Int
  field :event_location, types.String
  field :name, types.String
    field :user do
    type -> { UserType }

    resolve -> (group, args, ctx) {
      group.user
    }
  end

  connection :invites, -> { InviteType.connection_type } do
    resolve ->(group, args, ctx) {
      group.invites
    }
  end

# End of fields
end
