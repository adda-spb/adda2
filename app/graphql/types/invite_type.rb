InviteType = GraphQL::ObjectType.define do
  name 'Invite'
  description 'Invite type'

  interfaces [NodeIdentification.interface]
  
  global_id_field :id
  field :updated_at, types.String
  field :created_at, types.String
  field :sender_id, types.Int
  field :recipient_id, types.Int
  field :group_id, types.Int
    field :group do
    type -> { GroupType }

    resolve -> (invite, args, ctx) {
      invite.group
    }
  end

  field :sender do
    type -> { UserType }

    resolve -> (invite, args, ctx) {
      invite.sender
    }
  end

  field :recipient do
    type -> { UserType }

    resolve -> (invite, args, ctx) {
      invite.recipient
    }
  end

# End of fields
end
