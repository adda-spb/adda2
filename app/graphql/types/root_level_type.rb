RootLevelType = GraphQL::ObjectType.define do
  name 'RootLevel'
  description 'Unassociated root object queries'

  interfaces [NodeIdentification.interface]

  field :id, field: GraphQL::Relay::GlobalIdField.new('RootLevel')

  field :group, GroupType do
    argument :id, !types.ID
    resolve -> (obj, args, ctx) {
      Group.find(args[:id])
    }
  end

  connection :users, UserType.connection_type do
    resolve ->(object, args, ctx){
      User.all
    }
  end
  connection :groups, GroupType.connection_type do
    resolve ->(object, args, ctx){
      Group.all
    }
  end
  connection :invitations, InviteType.connection_type do
    resolve ->(object, args, ctx){
      Invite.all
    }
  end
end
