MutationType = GraphQL::ObjectType.define do
  name 'MutationType'
  field :createInvite, field: InviteMutations::Create.field
  field :updateInvite, field: InviteMutations::Update.field
  field :destroyInvite, field: InviteMutations::Destroy.field

  field :createGroup, field: GroupMutations::Create.field
  field :updateGroup, field: GroupMutations::Update.field
  field :destroyGroup, field: GroupMutations::Destroy.field

  field :createUser, field: UserMutations::Create.field
  field :updateUser, field: UserMutations::Update.field
  field :destroyUser, field: UserMutations::Destroy.field

end
