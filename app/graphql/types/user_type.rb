UserType = GraphQL::ObjectType.define do
  name 'User'
  description 'User type'

  interfaces [NodeIdentification.interface]

  global_id_field :id
  field :updated_at, types.String
  field :created_at, types.String
  field :password, types.String
  field :password_confirmation, types.String
  field :email, types.String
  field :mobile_number, types.String
  field :username, types.String
    connection :invitations, -> { InviteType.connection_type } do
    resolve ->(user, args, ctx) {
      user.invitations
    }
  end

  connection :sent_invites, -> { InviteType.connection_type } do
    resolve ->(user, args, ctx) {
      user.sent_invites
    }
  end

# End of fields
end
