module InviteMutations

  Create = GraphQL::Relay::Mutation.define do
    name 'CreateInvite'
    input_field :sender_id, !types.Int
    input_field :recipient_id, !types.Int
    input_field :group_id, !types.Int

    return_field :invite, InviteType

    resolve -> (inputs, ctx) {
      root = RootLevel::STATIC
      attr = inputs.keys.inject({}) do |memo, key|
        memo[key] = inputs[key] unless key == "clientMutationId"
        memo
      end

      invite = Invite.create(attr)

      { invite: invite }
    }
  end

  Update = GraphQL::Relay::Mutation.define do
    name 'UpdateInvite'
    input_field :sender_id, types.Int
    input_field :recipient_id, types.Int
    input_field :group_id, types.Int

    input_field :id, !types.ID

    return_field :invite, InviteType

    resolve -> (inputs, ctx) {
      invite = NodeIdentification.object_from_id((inputs[:id]), ctx)
      attr = inputs.keys.inject({}) do |memo, key|
        memo[key] = inputs[key] unless key == "clientMutationId" || key == 'id'
        memo
      end

      invite.update(attr)
      { invite: invite }
    }
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "DestroyInvite"

    input_field :id, !types.ID

    resolve -> (inputs, ctx) {
      invite = NodeIdentification.object_from_id((inputs[:id]), ctx)
      invite.destroy
      { }
    }
  end
end
