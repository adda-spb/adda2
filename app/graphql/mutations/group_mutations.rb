module GroupMutations

  Create = GraphQL::Relay::Mutation.define do
    name 'CreateGroup'
    input_field :user_id, !types.Int
    input_field :event_location, !types.String
    input_field :name, !types.String

    return_field :group, GroupType

    resolve -> (inputs, ctx) {
      root = RootLevel::STATIC
      attr = inputs.keys.inject({}) do |memo, key|
        memo[key] = inputs[key] unless key == "clientMutationId"
        memo
      end

      group = Group.create(attr)

      { group: group }
    }
  end

  Update = GraphQL::Relay::Mutation.define do
    name 'UpdateGroup'
    input_field :user_id, types.Int
    input_field :event_location, types.String
    input_field :name, types.String

    input_field :id, !types.ID

    return_field :group, GroupType

    resolve -> (inputs, ctx) {
      group = NodeIdentification.object_from_id((inputs[:id]), ctx)
      attr = inputs.keys.inject({}) do |memo, key|
        memo[key] = inputs[key] unless key == "clientMutationId" || key == 'id'
        memo
      end

      group.update(attr)
      { group: group }
    }
  end

  Destroy = GraphQL::Relay::Mutation.define do
    name "DestroyGroup"

    input_field :id, !types.ID

    resolve -> (inputs, ctx) {
      group = NodeIdentification.object_from_id((inputs[:id]), ctx)
      group.destroy
      { }
    }
  end
end
